package publisher

import listeners.EventListener
import java.io.File

import kotlin.collections.ArrayList

class EventManager() {

    var listners: MutableMap<String,ArrayList<EventListener>> = mutableMapOf<String,ArrayList<EventListener>>()

    constructor(vararg operations: String): this(){
        for (operation in operations){
            this.listners.put(operation, arrayListOf());
        }
    }

    public fun suscribe(eventType: String, listener: EventListener): Unit{
        var users = listners.get(eventType)
        users?.add(listener)
    }

    public fun unsusbscribe(eventType: String, listener: EventListener){
        var users = listners.get(eventType);
        users?.remove(listener)
    }

    public fun notify(eventType: String, file: File){
        var users = listners.get(eventType)
        if (users != null) {
            for(listener: EventListener in users){
                listener.update(eventType,file)
            }
        }
    }
}