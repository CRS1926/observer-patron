package listeners

import java.io.File

class LogOpenListener(): EventListener {

    private lateinit var log: File

    constructor(fileName: String) : this() {
        this.log = File(fileName);
    }

    override fun update(eventType: String, file: File) {
        println("Save to log $log: Someon has performed $eventType operation with the following file ${file.name}")
    }
}