package listeners

import java.io.File

class EmailNotificationListener() : EventListener {

    private lateinit var email: String

    constructor(email: String): this(){
        this.email = email
    }

    override fun update(eventType: String, file: File) {
        println("Email to ${email}: some has permfomed ${eventType} operation with following file ${file.name} ")
    }
}