package editor

import publisher.EventManager
import java.io.File

class Editor {

    public  lateinit var events: EventManager;
    private lateinit var file: File;

    constructor(){
        this.events = EventManager("open","save");
    }

    public fun openFile(filePath: String): Unit{
        this.file = File(filePath);
        events.notify("open",file)
    }

    @Throws(Exception::class)
    public fun saveFile(): Unit{
        if(this.file != null){
            events.notify("save",file);
        }else{
            throw Exception("Please open a file fist");
        }
    }

}