package Application

import editor.Editor
import listeners.EmailNotificationListener
import listeners.LogOpenListener
import kotlin.math.log
import java.io.IOException as IOException1


fun main(){
        var editor = Editor()
        editor.events.suscribe("open", LogOpenListener("../file.txt"));
        editor.events.suscribe("save", EmailNotificationListener("example@gmail.com"))

        try {
            editor.openFile("test.txt");
            editor.saveFile();
        } catch (e: IOException1){
            e.printStackTrace()
        }

    }

